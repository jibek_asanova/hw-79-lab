const express = require('express');
const mysqlDb = require('../mysqlDb');

const router = express.Router();

router.get('/', async (req, res) => {
  const [categories] = await mysqlDb.getConnection().query('SELECT * from categories');
  res.send(categories);
});


router.get('/:id', async (req, res) => {
  const [category] = await mysqlDb.getConnection().query(
    'SELECT * FROM ?? where id = ?',
    ['categories', req.params.id])
  if (!category) {
    return res.status(404).send({error: 'category not found'});
  }

  res.send(category[0]);
});

router.post('/', async (req, res) => {
  if (!req.body.title || !req.body.description) {
    return res.status(400).send({error: 'Data not valid'});
  }

  const category = {
    title: req.body.title,
    description: req.body.description,
  };

  const newCategory = await mysqlDb.getConnection().query(
    'INSERT INTO ?? (title, description) values (?, ?)',
    ['categories', category.title, category.description]
  );

  res.send({
    ...category,
    id: newCategory[0].insertId
  });
});

router.put('/:id', async (req, res) => {
  const category = {
    title: req.body.title,
    description: req.body.description,
  };

  await mysqlDb.getConnection().query(
    'UPDATE ?? SET ? where id = ?',
    ['categories', {...category}, req.params.id]);

  res.send({message: `Update successful, id= ${req.params.id}`});
});

router.delete('/:id', async (req, res) => {
  try{
    await mysqlDb.getConnection().query(
      'DELETE  FROM ??  where id = ?',
      ['categories', req.params.id]);
    res.send({message: `Delete successful, id= ${req.params.id}`});
  } catch (e) {
    return res.status(400).send({error: 'Error: Cannot delete or update a parent row'});
  }

});

module.exports = router;

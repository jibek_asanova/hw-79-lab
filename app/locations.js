const express = require('express');
const mysqlDb = require('../mysqlDb');

const router = express.Router();

router.get('/', async (req, res) => {
  const [locations] = await mysqlDb.getConnection().query('SELECT * from locations');
  res.send(locations);
});

router.get('/:id', async (req, res) => {
  const [location] = await mysqlDb.getConnection().query(
    'SELECT * FROM ?? where id = ?',
    ['locations', req.params.id])
  if (!location) {
    return res.status(404).send({error: 'location not found'});
  }

  res.send(location[0]);
});

router.post('/', async (req, res) => {
  if (!req.body.title || !req.body.description) {
    return res.status(400).send({error: 'Data not valid'});
  }

  const location = {
    title: req.body.title,
    description: req.body.description,
  };

  const newLocation = await mysqlDb.getConnection().query(
    'INSERT INTO ?? (title, description) values (?, ?)',
    ['locations', location.title, location.description]
  );

  res.send({
    ...location,
    id: newLocation[0].insertId
  });
});

router.put('/:id', async (req, res) => {
  const location = {
    title: req.body.title,
    description: req.body.description,
  };

  await mysqlDb.getConnection().query(
    'UPDATE ?? SET ? where id = ?',
    ['locations', {...location}, req.params.id]);

  res.send({message: `Update successful, id= ${req.params.id}`});
});

router.delete('/:id', async (req, res) => {
  try{
    await mysqlDb.getConnection().query(
      'DELETE  FROM ??  where id = ?',
      ['locations', req.params.id]);
    res.send({message: `Delete successful, id= ${req.params.id}`});
  } catch (e) {
    return res.status(400).send({error: `Error: Cannot delete or update a parent row, id=${req.params.id}`});
  }
});


module.exports = router;

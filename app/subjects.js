const express = require('express');
const mysqlDb = require('../mysqlDb');
const multer = require('multer');
const config = require('../config');
const path = require("path");
const {nanoid} = require("nanoid");


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  const [subjects] = await mysqlDb.getConnection().query('SELECT * from subjects');
  res.send(subjects);
});

router.get('/:id', async (req, res) => {
  const [subject] = await mysqlDb.getConnection().query(
    'SELECT * FROM ?? where id = ?',
    ['subjects', req.params.id])
  if (!subject) {
    return res.status(404).send({error: 'Subject not found'});
  }

  res.send(subject[0]);
});

router.post('/', upload.single('image'), async (req, res) => {
  if (!req.body.category_id || !req.body.location_id || !req.body.title || !req.body.description || !req.body.date_time) {
    return res.status(400).send({error: 'Data not valid'});
  }

  const subject = {
    category_id: req.body.category_id,
    location_id: req.body.location_id,
    title: req.body.title,
    description: req.body.description,
    date_time: req.body.date_time
  };

  if(req.file) {
    subject.image = req.file.filename;
  }

  const newSubject = await mysqlDb.getConnection().query(
    'INSERT INTO ?? (category_id, location_id, title, description, date_time, image) values (?, ?, ?, ?, ?, ?)',
    ['subjects', subject.category_id, subject.location_id, subject.title, subject.description, subject.date_time, subject.image]
  );

  res.send({
    ...subject,
    id: newSubject[0].insertId
  });
});

router.put('/:id', upload.single('image'), async (req, res) => {
  const subject = {
    category_id: req.body.category_id,
    location_id: req.body.location_id,
    title: req.body.title,
    description: req.body.description,
    date_time: req.body.date_time
  };

  if(req.file) subject.image = req.file.filename;

  await mysqlDb.getConnection().query(
    'UPDATE ?? SET ? where id = ?',
    ['subjects', {...subject}, req.params.id]);

  res.send({message: `Update successful, id= ${req.params.id}`});
});

router.delete('/:id', async (req, res) => {

  await mysqlDb.getConnection().query(
    'DELETE  FROM ??  where id = ?',
    ['subjects', req.params.id]);

  res.send({message: `Delete successful, id= ${req.params.id}`});
});


module.exports = router;
const path = require("path");
const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  databaseOptions: {
    host: 'localhost',
    user: 'userTest',
    password: 'test',
    database: 'inventory',
  }
};
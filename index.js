const express = require('express');
const subjects = require('./app/subjects');
const categories = require('./app/categories');
const locations = require('./app/locations');
const mysqlDb = require('./mysqlDb');

const app = express();
app.use(express.json());

const port = 8000;

app
  .use('/subjects', subjects)
  .use('/categories', categories)
  .use('/locations', locations);

mysqlDb.connect().catch(e => console.log(e));
app.listen(port, () => {
  console.log(`Server started on ${port} port!`);
});
DROP DATABASE IF EXISTS inventory;

CREATE DATABASE IF NOT EXISTS inventory;

use inventory;

CREATE TABLE categories (
    id int auto_increment,
    title varchar(255) not null,
    description text null,
    constraint  categories_pk primary key (id)
);

CREATE TABLE locations (
    id int auto_increment,
    title varchar(255) not null,
    description text null,
    constraint  location_pk primary key (id)
);

CREATE TABLE subjects (
    id int auto_increment,
    category_id int not null ,
    location_id int not null,
    title varchar(255) not null,
    description text null,
    date_time datetime null ,
    constraint  subjects_pk primary key (id),
    constraint subjects_categories_id_fk
    foreign key (category_id)
    references  categories (id)
    on update cascade
    on delete restrict,
    constraint subjects_locations_id_fk
    foreign key (location_id)
    references  locations (id)
    on update cascade
    on delete restrict
);

INSERT INTO categories (title, description)
VALUES ('мебель', 'мебель для офиса'),
       ('компьютерное оборудование', 'компьютерное оборудование для офиса'),
       ('концелярия', 'концелярия для офиса'),
       ('кухонные пренадлежности', null);

SELECT * FROM categories;

INSERT INTO locations (title, description)
VALUES ('Офис № 1', 'кабинет директора'),
       ('Офис № 2', 'офис для старших менеджеров'),
       ('Офис № 3', 'офис для бухгалтеров'),
       ('Офис № 4', 'офис для стажеров');

SELECT * FROM locations;

INSERT INTO subjects(category_id, location_id, title, description, date_time)
VALUES (1, 2, 'стулья', '12 стульев для офиса старших менеджеров', '2015-04-03 14:00:45'),
       (2, 3, 'компьтер', '2 компьютера в офис для бухгалетров', null),
       (3, 1, 'ручки', '3 ручки в офис для директора', null);

UPDATE subjects SET date_time = '2020-05-03 12:00:00' WHERE id = 2;
SELECT * FROM subjects;

SELECT * FROM subjects ORDER BY date_time DESC;




